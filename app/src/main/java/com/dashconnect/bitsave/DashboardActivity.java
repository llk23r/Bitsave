package com.dashconnect.bitsave;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

import java.util.Date;

public class DashboardActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.tv_budget_value)
    TextView budgetValue;

    @BindView(R.id.seekBar)
    SeekBar seekBar;

    @BindView(R.id.tv_balance_value)
    TextView balanceValue;

    @BindView(R.id.et_expense_value)
    EditText editExpenseValue;

    @BindView(R.id.spn_expense_reason)
    Spinner expenseReason;

    private SharedPreferences sharedPreferences;
    private dbConfig db;
    private AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
    public String access;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        seekBar.setOnSeekBarChangeListener(this);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        db = new dbConfig(this);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Arkhip_font.ttf");
        budgetValue.setTypeface(custom_font);
        balanceValue.setTypeface(custom_font);

    }


    @Override
    protected void onStart() {
        super.onStart();
        sharedPreferences = getSharedPreferences("UserDetails", MODE_PRIVATE);
        updateBudgetValue();
    }

    private void updateBudgetValue() {
        String budget = sharedPreferences.getString("budget", "0");
        budgetValue.setText(budget);
        balanceValue.setText(budget);
    }

    private void updateBalanceValue() {
        String budget = sharedPreferences.getString("balance", "0");
        balanceValue.setText(budget);
    }

    @OnClick(R.id.btn_history)
    public void openHistoryActivity(View view) {
        startActivity(new Intent(this, HistoryActivity.class));
    }

    @OnClick(R.id.btn_invest)
    public void openInvestActivity(View view) {

        Intent i = new Intent(this, InvestActivity.class);

        // i.putExtra("access",val);

        startActivity(i);
    }

    @OnClick(R.id.btn_budget)
    public void openSetBudgetActivity(View view) {
        startActivity(new Intent(this, SetBudgetActivity.class));
    }

    @OnClick(R.id.btn_passbook)
    public void openPassbookActivity(View view) {
        startActivity(new Intent(this, PassbookActivity.class));
    }

    @OnClick(R.id.btn_MyInvest)
    public void openInvestmentsActivity(View view) {
        startActivity(new Intent(this, InvestmentsActivity.class));
    }

    @OnClick(R.id.btn_add_expense)
    public void addExpense(View view) {

        String balance = sharedPreferences.getString("balance", "0");
        String expense = editExpenseValue.getText().toString();
        String reason = expenseReason.getSelectedItem().toString();

        db.addExpense(new Date().toString(), Double.parseDouble(expense), reason);

        int expenseInteger = Integer.parseInt(expense);
        int balanceInteger = Integer.parseInt(balance);
        balanceInteger -= expenseInteger;


        editExpenseValue.setText("0");
        seekBar.setProgress(0);

        sharedPreferences.edit().putString("balance", String.valueOf(balanceInteger)).apply();

        updateBalanceValue();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Added Expense Successfully!")
                .setCancelable(false)
                .setPositiveButton("OK", null);
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        editExpenseValue.setText(String.valueOf(i * 10));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
