package com.dashconnect.bitsave;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.codecrafters.tableview.TableHeaderAdapter;
import de.codecrafters.tableview.TableView;
import de.codecrafters.tableview.toolkit.SimpleTableDataAdapter;
import de.codecrafters.tableview.toolkit.SimpleTableHeaderAdapter;
import de.codecrafters.tableview.toolkit.TableDataRowBackgroundProviders;

import java.util.List;

public class HistoryActivity extends AppCompatActivity {

    private dbConfig db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        db = new dbConfig(this);
        List<expenses> dataList =  db.getAllexpenses();

        String[][] data = new String[dataList.size()][3];

        for(int i=0; i<dataList.size(); i++){
            data[i][0] = dataList.get(i).getDate();
            data[i][1] = String.valueOf(dataList.get(i).getInr());
            data[i][2] = dataList.get(i).getType();
        }


        TableView<String[]> tableView = (TableView<String[]>) findViewById(R.id.tableView);
        tableView.setDataRowBackgroundProvider(TableDataRowBackgroundProviders.similarRowColor(Color.WHITE));
        tableView.setHeaderAdapter(new SimpleTableHeaderAdapter(this, "Date", "Reason", "Price"));
        tableView.setDataAdapter(new SimpleTableDataAdapter(this, data));
    }
}
