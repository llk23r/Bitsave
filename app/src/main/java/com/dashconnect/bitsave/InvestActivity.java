package com.dashconnect.bitsave;

import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class InvestActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.tv_budget_value)
    TextView budgetValue;

    @BindView(R.id.tv_balance_value)
    TextView balanceValue;

    @BindView(R.id.tv_expense_value)
    TextView expenseValue;

    @BindView(R.id.et_invest_value)
    EditText investValue;

    @BindView(R.id.seekBar)
    SeekBar seekBar;

    @BindView(R.id.btn_invest)
    Button btnInvest;

    private SharedPreferences sharedPreferences ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invest);
        ButterKnife.bind(this);

        sharedPreferences=getSharedPreferences("UserDetails",MODE_PRIVATE);
        String budget = sharedPreferences.getString("budget","0");
        String balance = sharedPreferences.getString("balance","0");
        budgetValue.setText(budget);
        balanceValue.setText(balance);

        String expense = String.valueOf(Integer.parseInt(budget) - Integer.parseInt(balance));
        expenseValue.setText(expense);
        investValue.setText(balance);

        seekBar.setOnSeekBarChangeListener(this);

        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/Arkhip_font.ttf");
        budgetValue.setTypeface(custom_font);
        balanceValue.setTypeface(custom_font);
        expenseValue.setTypeface(custom_font);
        btnInvest.setTypeface(custom_font);

    }

    @OnClick(R.id.btn_invest)
    public void invest(){

        String value = investValue.getText().toString();
        new InvestmentsActivity().buybtc(value);


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Successfully Invested!")
                .setCancelable(false)
                .setPositiveButton("OK", null);
        AlertDialog alert = builder.create();
        alert.show();
    }



    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        int balance = Integer.parseInt(sharedPreferences.getString("balance","0"));
        investValue.setText(String.valueOf(i*(balance/100)));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
