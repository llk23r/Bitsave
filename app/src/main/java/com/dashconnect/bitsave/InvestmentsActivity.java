package com.dashconnect.bitsave;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class InvestmentsActivity extends AppCompatActivity {

    @BindView(R.id.btn_withdraw)
    Button btnWithdraw;
    private AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
    TextView btc,investment,profit;
    public static String access,response;
    public static String current_rate;


    InvestmentsActivity(){
        login();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investments);
        ButterKnife.bind(this);


        btc=(TextView) findViewById(R.id.btc);
        investment=(TextView) findViewById(R.id.investment);
        profit=(TextView) findViewById(R.id.profit);







    }


    public String login() {
        RequestParams requestParams = new RequestParams();
        requestParams.put("email", "team25@gmail.com");
        requestParams.put("pass", "rYr3ejuD");
        requestParams.put("otp", "999999");
        asyncHttpClient.get("http://focusindiaministries.com/bitsave/signin.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                response=new String(responseBody);
                InvestmentsActivity.access = response;
                InvestmentsActivity.this.currentRate(access);
                InvestmentsActivity.this.transactions();
                transactions();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });
        return response;
    }


   public String currentRate( String accesscode){

        RequestParams requestParams = new RequestParams();


        requestParams.put("access", accesscode);
        asyncHttpClient.get("http://focusindiaministries.com/bitsave/wallet.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {



                try {
                    JSONObject jsonRootObject = new JSONObject(new String(responseBody) );
                    current_rate=jsonRootObject.getString("sellbtc");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });


        return current_rate;
    }


    public void transactions(){

        RequestParams requestParams = new RequestParams();
        requestParams.put("access", access);

        RequestHandle requestHandle = asyncHttpClient.get("http://focusindiaministries.com/bitsave/transactions.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String resp = new String(responseBody);
                JSONObject jsonRootObject;
                try {
                    jsonRootObject = new JSONObject(resp);
                    //Get the instance of JSONArray that contains JSONObjects
                    JSONArray jsonArray = jsonRootObject.optJSONArray("transactions");

                    double x = 0.0, y = 0.0;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String exchange_rate = jsonObject.optString("exchange_rate");
                        String btc = jsonObject.optString("btc");
                        String date = jsonObject.optString("date");
                        String inr = jsonObject.optString("amount");
                        String status = jsonObject.optString("status");
                        x += Double.parseDouble(inr);
                        y += Double.parseDouble(btc);
                    }
                    String val = currentRate(access);
                    Double latest = y * (Double.parseDouble(val));
                    Double diff = latest - x;
                    btc.setText(String.valueOf(y));
                    investment.setText(String.valueOf(x));
                    profit.setText(String.valueOf(diff));


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Get the instance of JSONArray that contains JSONObjects
                // JSONArray jsonArray = jsonRootObject.optJSONArray("data");


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });


    }


    public void buybtc(String btc){

        RequestParams requestParams = new RequestParams();
        requestParams.put("access", access);
        requestParams.put("btc",btc);

        asyncHttpClient.get("http://focusindiaministries.com/bitsave/buybtc.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String resp=new String(responseBody);
                JSONObject jsonRootObject;
                try {
                    jsonRootObject = new JSONObject(resp);

                    String status = jsonRootObject.optString("message");
                    String result = jsonRootObject.optString("result");
                    if(result=="success"){

                    }
                    Toast.makeText(InvestmentsActivity.this,status,Toast.LENGTH_SHORT).show();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }

            //Get the instance of JSONArray that contains JSONObjects
            // JSONArray jsonArray = jsonRootObject.optJSONArray("data");


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });


    }


    @OnClick(R.id.btn_withdraw)
    public void withdrawBitcoins(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you wish to withdraw your Bitcoin investments?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ProgressDialog pd = new ProgressDialog(InvestmentsActivity.this);
                        pd.setMessage("WIthdrawing Bitcoins");
                        pd.show();
                        AlertDialog.Builder builder = new AlertDialog.Builder(InvestmentsActivity.this);
                        builder.setMessage("Your Bitcoins have been successfully withdrawn.")
                                .setCancelable(false)
                                .setPositiveButton("OK", null).create().show();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener(){

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }
}
