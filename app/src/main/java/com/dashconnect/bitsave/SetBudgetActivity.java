package com.dashconnect.bitsave;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetBudgetActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    @BindView(R.id.seekBar)
    SeekBar seekBar;

    @BindView(R.id.et_budgetValue)
    EditText editBudgetValue;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_budget);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences("UserDetails", MODE_PRIVATE);
        seekBar.setOnSeekBarChangeListener(this);


    }

    @OnClick(R.id.btn_set_budget)
    public void setBudget(View view){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String s1 = editBudgetValue.getText().toString();
        editor.putString("budget", s1);
        editor.putString("balance", s1);
        editor.apply();
        finish();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        editBudgetValue.setText(String.valueOf(i*10000));
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
