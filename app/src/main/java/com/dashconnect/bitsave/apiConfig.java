package com.dashconnect.bitsave;

import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by root on 9/4/17.
 */

public class apiConfig {

    private AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
    public static String access,response;
    public static String current_rate;


    public String login() {
        RequestParams requestParams = new RequestParams();
        requestParams.put("email", "team25@gmail.com");
        requestParams.put("pass", "rYr3ejuD");
        requestParams.put("otp", "999999");
        asyncHttpClient.get("http://focusindiaministries.com/bitsave/signin.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                response=new String(responseBody);

                //Toast.makeText(InvestmentsActivity.this,response,Toast.LENGTH_LONG).show();


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });
        return response;
    }


    public String currentRate(){

        RequestParams requestParams = new RequestParams();
       // Toast.makeText(InvestmentsActivity.this, access, Toast.LENGTH_LONG).show();

        requestParams.put("access", access);
        asyncHttpClient.get("http://focusindiaministries.com/bitsave/wallet.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {



                try {
                    JSONObject jsonRootObject = new JSONObject(new String(responseBody) );
                    current_rate=jsonRootObject.getString("sellbtc");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
             //   Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });


        return current_rate;
    }


    public void transactions(){

        RequestParams requestParams = new RequestParams();
        requestParams.put("access", access);

        asyncHttpClient.get("http://focusindiaministries.com/bitsave/transactions.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String resp=new String(responseBody);
                JSONObject jsonRootObject;
                try {
                    jsonRootObject = new JSONObject(resp);
                    //Get the instance of JSONArray that contains JSONObjects
                    JSONArray jsonArray = jsonRootObject.optJSONArray("transactions");

                    double x=0.0,y=0.0;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String exchange_rate = jsonObject.optString("exchange_rate");
                        String btc = jsonObject.optString("btc");
                        String date = jsonObject.optString("date");
                        String inr = jsonObject.optString("amount");
                        String status = jsonObject.optString("status");
                        x+=Double.parseDouble(inr);
                        y+=Double.parseDouble(btc);
                    }
                    String val=currentRate();
                    Double latest= y* (Double.parseDouble(val));
                    Double diff=latest-x;
                  //  Toast.makeText(InvestmentsActivity.this,diff.toString(),Toast.LENGTH_SHORT).show();


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Get the instance of JSONArray that contains JSONObjects
                // JSONArray jsonArray = jsonRootObject.optJSONArray("data");


            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
               // Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });


    }


    public void buybtc(String access,String btc){

        RequestParams requestParams = new RequestParams();
        requestParams.put("access", access);
        requestParams.put("btc",btc);

        asyncHttpClient.get("http://focusindiaministries.com/bitsave/buybtc.php", requestParams, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                String resp=new String(responseBody);
                JSONObject jsonRootObject;
                try {
                    jsonRootObject = new JSONObject(resp);

                    String status = jsonRootObject.optString("message");
                    String result = jsonRootObject.optString("result");
                    if(result=="success"){

                    }
                 //   Toast.makeText(InvestmentsActivity.this,status,Toast.LENGTH_SHORT).show();

                } catch (JSONException e1) {
                    e1.printStackTrace();
                }
            }

            //Get the instance of JSONArray that contains JSONObjects
            // JSONArray jsonArray = jsonRootObject.optJSONArray("data");


            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                //Toast.makeText(InvestmentsActivity.this, "failed!", Toast.LENGTH_LONG).show();
            }


        });


    }

}
