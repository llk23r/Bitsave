package com.dashconnect.bitsave;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.Console;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 9/4/17.
 */

public class dbConfig extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "bitsave";
    private static final String TABLE_EXP = "expenses";
    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_INR = "inr";
    private static final String KEY_TYPE = "type";


    public dbConfig(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_EXP + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_DATE + " DATE,"
                + KEY_INR + " REAL,"
                + KEY_TYPE + " TEXT" + ")";

        System.out.print(CREATE_TABLE);
        db.execSQL(CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EXP);
        // Create tables again
        onCreate(db);

    }

    void addExpense(String date, double inr, String type) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, date);
        values.put(KEY_INR, inr);
        values.put(KEY_TYPE, '"' + type + '"');

        // Inserting Row
        db.insert(TABLE_EXP, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get all expensess in a list view  
    public List<expenses> getAllexpenses() {

        List<expenses> expensesList = new ArrayList<expenses>();
        // Select All Query  
        String selectQuery = "SELECT  * FROM " + TABLE_EXP;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list  
        if (cursor.moveToFirst()) {
            do {
                expenses expenses = new expenses();
                expenses.setDate(cursor.getString(1));
                expenses.setInr(Double.parseDouble(cursor.getString(2)));
                expenses.setType(cursor.getString(3));
                // Adding expenses to list  
                expensesList.add(expenses);
            } while (cursor.moveToNext());
        }

        // return expenses list  
        return expensesList;
    }

}
